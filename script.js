class Address {
  constructor({continent, country, regionName, city, district}) {
      this.continent = continent,
      this.country = country,
      this.regionName = regionName,
      this.city = city,
      this.district = district
  }

  render() {
      const card = document.createElement('div');
      card.classList.add('addressCard');
      card.innerHTML = `
      <p>Континент: ${this.continent}</p>
      <p>Країна: ${this.country}</p>
      <p>Регіон: ${this.regionName}</p>
      <p>Місто: ${this.city}</p>
      <p>Район: ${this.district}</p>
      `;
      findAddressBtn.after(card);
  }
}

async function getIP() {
  const response = await fetch('https://api.ipify.org/?format=json');
  const ip = await response.json();

  async function getAddress(ip) {
    const response = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`);
    const address = await response.json();
    let card = new Address (address);
    card.render();
  }

  getAddress(ip.ip);
}

const findAddressBtn = document.querySelector("#find");
findAddressBtn.addEventListener("click", getIP);